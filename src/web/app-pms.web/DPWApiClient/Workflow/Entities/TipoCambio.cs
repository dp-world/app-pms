﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class TipoCambio
    {
        public TipoCambio()
        {
        }
        public string codigo { get; set; }
        public string nombreTipoCambio { get; set; }
    }
}