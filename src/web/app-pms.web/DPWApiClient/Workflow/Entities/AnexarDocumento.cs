﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class AnexarDocumento
    {
        public AnexarDocumento()
        {
        }

        [JsonProperty(PropertyName = "documentoAnexo")]
        public string DocumentoBase64 { get; set; }

        [JsonProperty(PropertyName = "idDocumentoPrincipal")]
        public string IdDocPrincipal { get; set; }

        [JsonProperty(PropertyName = "tituloDocumento")]
        public string TituloDocumento { get; set; }
    }
}