﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class ReportListadoDocsObsoletos
    {
        public ReportListadoDocsObsoletos()
        {
        }

        [JsonProperty(PropertyName = "aprobadoPor")]
        public string AprobadoPor { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }

        [JsonProperty(PropertyName = "fechaEfectiva")]
        public string FechaEfectiva { get; set; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "nombreDocumento")]
        public string NombreDocumento { get; set; }

        [JsonProperty(PropertyName = "nomenclatura")]
        public string Nomenclatura { get; set; }

        [JsonProperty(PropertyName = "revision")]
        public string Revision { get; set; }

        public string result { get; set; }

        public ReportListadoDocsObsoletos(string result)
        {
            this.result = result;
        }
    }
}