﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class TiempoRetencion
    {
        public TiempoRetencion()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoTiempoRetencion { get; set; }

        [JsonProperty(PropertyName = "nombreTiempoRetencion")]
        public string NombreTiempoRetencion { get; set; }

        public string result { get; set; }

        public TiempoRetencion(string result)
        {
            this.result = result;
        }

    }
}