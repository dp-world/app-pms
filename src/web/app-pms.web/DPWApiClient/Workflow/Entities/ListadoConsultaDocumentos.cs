﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class ListadoConsultaDocumentos
    {
        public ListadoConsultaDocumentos()
        {
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "nombre")]
        public string Nombre { get; set; }

        [JsonProperty(PropertyName = "version")]
        public string Version { get; set; }

        [JsonProperty(PropertyName = "solicitante")]
        public string Solicitante { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }
        
        [JsonProperty(PropertyName = "nomenclatura")]
        public string Nomenclatura { get; set; }

        [JsonProperty(PropertyName = "fechaCreacion")]
        public string FechaCreacion { get; set; }

        [JsonProperty(PropertyName = "tipoDocumento")]
        public string TipoDocumento { get; set; }

        [JsonProperty(PropertyName = "tipoCambio")]
        public string TipoCambio { get; set; }

        [JsonProperty(PropertyName = "razonCreacion")]
        public string RazonCreacion { get; set; }

        [JsonProperty(PropertyName = "tiempoRetencion")]
        public string TiempoRetencion { get; set; }

        [JsonProperty(PropertyName = "liderProceso")]
        public string LiderProceso { get; set; }

        [JsonProperty(PropertyName = "frecuenciaRevision")]
        public string FrecuenciaRevision { get; set; }

        [JsonProperty(PropertyName = "revision")]
        public string Revision { get; set; }

        [JsonProperty(PropertyName = "responsable")]
        public string Responsable { get; set; }

        [JsonProperty(PropertyName = "fechaProximaRevision")]
        public string FechaProximaRevision { get; set; }

        [JsonProperty(PropertyName = "clasificacion")]
        public string Clasificacion { get; set; }

        [JsonProperty(PropertyName = "fechaVencimiento")]
        public string FechaVencimiento { get; set; }

        public string result { get; set; }

        public ListadoConsultaDocumentos(string result)
        {
            this.result = result;
        }

    }
}