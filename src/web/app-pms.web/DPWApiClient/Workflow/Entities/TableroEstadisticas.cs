﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class TableroEstadisticas
    {
        public TableroEstadisticas()
        {
        }
        
        [JsonProperty(PropertyName = "cantidadCreacion")]
        public string CantidadCreacion { get; set; }

        [JsonProperty(PropertyName = "cantidadActualizacion")]
        public string CantidadActualizacion { get; set; }

        [JsonProperty(PropertyName = "cantidadObsoletizacion")]
        public string CantidadObsoletizacion { get; set; }

        [JsonProperty(PropertyName = "cantidadReactivacion")]
        public string CantidadReactivacion { get; set; }

        [JsonProperty(PropertyName = "cantidadNomenclatura")]
        public string CantidadNomenclatura { get; set; }

        public string result { get; set; }
        public TableroEstadisticas(string result)
        {
            this.result = result;
        }
    }
}