﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class FrecuenciaRevision
    {
        public FrecuenciaRevision()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoFrecuenciaRevision { get; set; }

        [JsonProperty(PropertyName = "nombreFrecuenciaRevision")]
        public string NombreFrecuenciaRevision { get; set; }

        public string result { get; set; }

        public FrecuenciaRevision(string result)
        {
            this.result = result;
        }
    }
}