﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class WFDocumentSaveApproveRejectInput : WFDocumentCreateInput
    {
        public string url_paramKey1 { get; set; }
        public string url_paramValue1 { get; set; }
        public string url_paramKey2 { get; set; }
        public string url_paramValue2 { get; set; }
        public string url_paramKey3 { get; set; }
        public string url_paramValue3 { get; set; }
        public string url_paramKey4 { get; set; }
        public string url_paramValue4 { get; set; }
        public string url_paramKey5 { get; set; }
        public string url_paramValue5 { get; set; }
    }
}