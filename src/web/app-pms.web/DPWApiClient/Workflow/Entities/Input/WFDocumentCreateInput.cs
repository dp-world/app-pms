﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class WFDocumentCreateInput : WFCommon
    {
        public string header_key { get; set; }
        public string header_value { get; set; }
    }
}