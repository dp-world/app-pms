﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class WFCommon
    {
        public string url { get; set; }
        public string header_authType { get; set; }
        public string header_authName { get; set; }
        public string access_token { get; set; }
        
    }
}
