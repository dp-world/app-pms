﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class WFDocumentSearchDocsNameInput : WFCommon
    {
        public string docnameKey { get; set; }
        public string obsoleteKey { get; set; }
    }
}