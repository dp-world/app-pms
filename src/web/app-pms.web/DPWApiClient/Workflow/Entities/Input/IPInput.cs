﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class IPInput
    {
        public string flag_url_dpw { get; set; }
        public string url_dev { get; set; }
        public string url_prd { get; set; }
        public string url_api_searchDocName { get; set; }
    }
}