﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities.Input
{
    public class WFDocumentSelectInput : WFCommon
    {
        public string url_paramKey1 { get; set; }
        public string url_paramValue1 { get; set; }
        public string url_paramKey2 { get; set; }
        public string url_paramValue2 { get; set; }
    }
}
