﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class Departamento
    {
        public Departamento()
        {   
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoDepartamento { get; set; }

        [JsonProperty(PropertyName = "nombreDepartamento")]
        public string NombreDepartamento { get; set; }

        public string result { get; set; }

        public Departamento(string result)
        {
            this.result = result;
        }
    }
}