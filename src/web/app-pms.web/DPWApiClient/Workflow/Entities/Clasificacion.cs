﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class Clasificacion
    {
        public Clasificacion()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoClasificacion { get; set; }

        [JsonProperty(PropertyName = "nombreClasificacion")]
        public string NombreClasificacion { get; set; }

        public string result { get; set; }

        public Clasificacion(string result)
        {
            this.result = result;
        }
    }
}