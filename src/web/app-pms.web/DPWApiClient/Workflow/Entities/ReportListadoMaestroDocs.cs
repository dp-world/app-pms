﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class ReportListadoMaestroDocs
    {
        public ReportListadoMaestroDocs()
        {
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }

        [JsonProperty(PropertyName = "nomenclatura")]
        public string Nomenclatura { get; set; }

        [JsonProperty(PropertyName = "revision")]
        public string Revision { get; set; }

        [JsonProperty(PropertyName = "descripcionUltimoCambio")]
        public string DescripcionUltimoCambio { get; set; }

        [JsonProperty(PropertyName = "fechaAprobacion")]
        public string FechaAprobacion { get; set; }
        
        [JsonProperty(PropertyName = "clasificacion")]
        public string Clasificacion { get; set; }

        [JsonProperty(PropertyName = "aprobadoPor")]
        public string AprobadoPor { get; set; }

        [JsonProperty(PropertyName = "nombreDocumento")]
        public string NombreDocumento { get; set; }

        public string result { get; set; }

        public ReportListadoMaestroDocs(string result)
        {
            this.result = result;
        }
    }
}