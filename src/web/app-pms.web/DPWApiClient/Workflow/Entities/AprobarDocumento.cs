﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class AprobarDocumento
    {
        public AprobarDocumento()
        {
            Accion = "APROBAR";
        }
        [JsonProperty(PropertyName = "tipoDeAccion")]
        public string Accion { get; set; }
    }
}