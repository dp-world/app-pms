﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class BandejaTrabajo
    {
        public BandejaTrabajo()
        {
        }

        [JsonProperty(PropertyName = "cantidad")]
        public int Cantidad { get; set; }

        [JsonProperty(PropertyName = "listaColaDeTrabajo")]
        public List<ListadoBandejaTrabajo> ListadoBandejaTrabajo { get; set; }

        public string result { get; set; }

        public BandejaTrabajo(string result)
        {
            this.result = result;
        }
    }
}