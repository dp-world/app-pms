﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class ListadoBandejaTrabajo
    {
        [JsonProperty(PropertyName="id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "idPaso")]
        public string IdPaso { get; set; }

        [JsonProperty(PropertyName = "dDocName")]
        public string DocName { get; set; }

        [JsonProperty(PropertyName = "dRevLabel")]
        public string NumeroRevisionDocumento { get; set; }

        [JsonProperty(PropertyName = "dSecurityGroup")]
        public string GrupoSeguridad { get; set; }

        [JsonProperty(PropertyName = "tipoSolicitud")]
        public string TipoSolicitud { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }

        [JsonProperty(PropertyName = "fecha")]
        public string Fecha { get; set; }

        [JsonProperty(PropertyName = "nombrePaso")]
        public string NombrePaso { get; set; }

        [JsonProperty(PropertyName = "requiereSoporte")]
        public bool RequiereSoporte { get; set; }

        [JsonProperty(PropertyName = "nombreDocumento")]
        public string NombreDocumento { get; set; }

        [JsonProperty(PropertyName = "listaUsuarios")]
        public List<string> ListaUsuarios { get; set; }
    }
}