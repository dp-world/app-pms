﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class CrearDocumento
    {
        public CrearDocumento()
        {
        }
        
        [JsonProperty(PropertyName = "solicitante")]
        public string Solicitante { get; set; }

        [JsonProperty(PropertyName = "documentobase64")]
        public string DocumentoBase64 { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public Departamento Departamento { get; set; }

        [JsonProperty(PropertyName = "requiereSoporte")]
        public bool RequiereSoporte { get; set; }

        [JsonProperty(PropertyName = "requiereAdiestramiento")]
        public bool RequiereAdiestramiento { get; set; }

        [JsonProperty(PropertyName = "razonFuenteDeCreacion")]
        public RazonesCreacion RazonesCreacion { get; set; }

        [JsonProperty(PropertyName = "tipoDocumento")]
        public TipologiaDocumental TipologiaDocumental { get; set; }

        [JsonProperty(PropertyName = "tipoSolicitud")]
        public string TipoSolicitud { get; set; }

        [JsonProperty(PropertyName = "tipoCambio")]
        public string TipoCambio { get; set; }

        [JsonProperty(PropertyName = "tituloDocumento")]
        public string TituloDocumento { get; set; }
        public IFormFile FormFile { get; set; }
        public IFormFile FormFileAnnexed { get; set; }

        [JsonProperty(PropertyName = "justificacion")]
        public string Justificacion { get; set; }

        [JsonProperty(PropertyName = "fechaVencimiento")]
        public string FechaVencimiento { get; set; }
        public int Id { get; set; }
        public int IdPaso { get; set; }

        [JsonProperty(PropertyName = "dDocName")]
        public string DocName { get; set; }

        [JsonProperty(PropertyName = "dRevLabel")]
        public string NumeroRevisionDocumento { get; set; }

        [JsonProperty(PropertyName = "dSecurityGroup")]
        public string GrupoSeguridad { get; set; }

        [JsonProperty(PropertyName = "nombreDocumento")]
        public string NombreDocumento { get; set; }
        public string NombrePaso { get; set; }

        [JsonProperty(PropertyName = "tiempoRetencion")]
        public string TiempoRetencion { get; set; }

        [JsonProperty(PropertyName = "medioAlmacenamiento")]
        public string MedioAlmacenamiento { get; set; }

        [JsonProperty(PropertyName = "revision")]
        public string Revision { get; set; }

        [JsonProperty(PropertyName = "responsable")]
        public string Responsable { get; set; }

        [JsonProperty(PropertyName = "clasificacion")]
        public string Clasificacion { get; set; }

        [JsonProperty(PropertyName = "fechaProximaRevision")]
        public string FechaProximaRevision { get; set; }

        [JsonProperty(PropertyName = "liderProceso")]
        public string LiderProceso { get; set; }

        [JsonProperty(PropertyName = "frecuenciaRevision")]
        public string FrecuenciaRevision { get; set; }

        [JsonProperty(PropertyName = "documentoAnexo")]
        public string documentoAnexo { get; set; }
                
        [JsonProperty(PropertyName = "tituloDocumentoAnexo")]
        public string TituloDocumentoAnexo { get; set; }

        [JsonProperty(PropertyName = "areaActual")]
        public string AreaActual { get; set; }

        [JsonProperty(PropertyName = "areaPropuesta")]
        public string AreaPropuesta { get; set; }

        [JsonProperty(PropertyName = "cambioTipoDocumentoDe")]
        public string CambioTipoDocumentoDe { get; set; }

        [JsonProperty(PropertyName = "cambioTipoDocumentoA")]
        public string CambioTipoDocumentoA { get; set; }

        [JsonProperty(PropertyName = "razonCambio")]
        public string RazonDelCambio { get; set; }

        [JsonProperty(PropertyName = "nomenclatura")]
        public string Nomenclatura { get; set; }

        [JsonProperty(PropertyName = "compartirDocumentoCon")]
        public string CompartirDocumentoCon { get; set; }

        public string result { get; set; }

        public CrearDocumento(string result)
        {
            this.result = result;
        }
    }
}