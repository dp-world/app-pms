﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class MedioAlmacenamiento
    {
        public MedioAlmacenamiento()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoMedioAlmacenamiento { get; set; }

        [JsonProperty(PropertyName = "nombreMedioAlmacenamiento")]
        public string NombreMedioAlmacenamiento { get; set; }

        public string result { get; set; }

        public MedioAlmacenamiento(string result)
        {
            this.result = result;
        }
    }
}