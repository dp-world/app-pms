﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class RechazarDocumento : AprobarDocumento
    {
        public RechazarDocumento()
        {
            Accion = "RECHAZAR";
        }
        [JsonProperty(PropertyName = "razonRechazo")]
        public string RazonRechazo { get; set; }
    }
}