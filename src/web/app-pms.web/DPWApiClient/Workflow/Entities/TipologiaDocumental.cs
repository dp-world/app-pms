﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class TipologiaDocumental
    {
        public TipologiaDocumental()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoTipoDocumental { get; set; }

        [JsonProperty(PropertyName = "nombreTipologia")]
        public string NombreTipoDocumental { get; set; }

        public string result { get; set; }

        public TipologiaDocumental(string result)
        {
            this.result = result;
        }
    }
}