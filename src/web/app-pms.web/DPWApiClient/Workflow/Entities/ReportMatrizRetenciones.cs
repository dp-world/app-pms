﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class ReportMatrizRetenciones
    {
        public ReportMatrizRetenciones()
        {
        }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }

        [JsonProperty(PropertyName = "noRegistro")]
        public string NoRegistro { get; set; }

        [JsonProperty(PropertyName = "nombreRegistro")]
        public string NombreRegistro { get; set; }

        [JsonProperty(PropertyName = "tiempoDeRetencion")]
        public string TiempoRetencion { get; set; }

        [JsonProperty(PropertyName = "medioAlmacenamiento")]
        public string MedioAlmacenamiento { get; set; }

        [JsonProperty(PropertyName = "responsable")]
        public string Responsable { get; set; }

        public string result { get; set; }

        public ReportMatrizRetenciones(string result)
        {
            this.result = result;
        }
    }
}