﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Entities
{
    public class RazonesCreacion
    {
        public RazonesCreacion()
        {
        }

        [JsonProperty(PropertyName = "codigo")]
        public string CodigoRazonCreacion { get; set; }

        [JsonProperty(PropertyName = "nombreRazonFuenteCreacion")]
        public string NombreRazonCreacion { get; set; }

        public string result { get; set; }

        public RazonesCreacion(string result)
        {
            this.result = result;
        }
    }
}