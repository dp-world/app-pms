﻿using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities;
using App_Pms.Web.DPWApiClient.Workflow.Entities;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Workflow.Manager
{
    public static class Common
    {
        #region "GET_METHODs"
        public static async Task<BandejaTrabajo> GetWorkQueueAsync(WFQueueInput wFQueueInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFQueueInput.header_authName, wFQueueInput.header_authType + wFQueueInput.access_token);

            var client = new RestClient(wFQueueInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<BandejaTrabajo>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new BandejaTrabajo(result.status + "|" + result.message);
        }

        public static async Task<List<ListadoConsultaDocumentos>> GetListDocsQueueAsync(WFDocumentSelectListDocsInput wFDocumentSelectListDocsInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFDocumentSelectListDocsInput.header_authName, wFDocumentSelectListDocsInput.header_authType + wFDocumentSelectListDocsInput.access_token);

            var client = new RestClient(wFDocumentSelectListDocsInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<ListadoConsultaDocumentos>>(response.Content);
            
            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<ListadoConsultaDocumentos> { new ListadoConsultaDocumentos(result.status + "|" + result.message) };
        }

        public static async Task<List<string>> GetListSearchDocsNamesAsync(WFDocumentSearchDocsNameInput wFDocumentSearchDocsNameInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFDocumentSearchDocsNameInput.header_authName, wFDocumentSearchDocsNameInput.header_authType + wFDocumentSearchDocsNameInput.access_token);

            var client = new RestClient(wFDocumentSearchDocsNameInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<string>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<string> { result.status + "|" + result.message };
        }

        public static async Task<List<ReportMatrizRetenciones>> GetReportMatrizRetencionesAsync(WFDocumentReportMatrizRetencionesInput wFDocumentReportMatrizRetencionesInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFDocumentReportMatrizRetencionesInput.header_authName, wFDocumentReportMatrizRetencionesInput.header_authType + wFDocumentReportMatrizRetencionesInput.access_token);

            var client = new RestClient(wFDocumentReportMatrizRetencionesInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<ReportMatrizRetenciones>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<ReportMatrizRetenciones> { new ReportMatrizRetenciones(result.status + "|" + result.message) };
        }

        public static async Task<List<ReportListadoMaestroDocs>> GetReportListMastDocsAsync(WFDocumentReportListadoMaestroDocsInput wFDocumentReportListadoMaestroDocsInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFDocumentReportListadoMaestroDocsInput.header_authName, wFDocumentReportListadoMaestroDocsInput.header_authType + wFDocumentReportListadoMaestroDocsInput.access_token);

            var client = new RestClient(wFDocumentReportListadoMaestroDocsInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<ReportListadoMaestroDocs>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<ReportListadoMaestroDocs> { new ReportListadoMaestroDocs(result.status + "|" + result.message) };
        }

        public static async Task<List<ReportListadoDocsObsoletos>> GetReportListObsoletesDocsAsync(WFDocumentReportListadoDocsObsoletosInput wFDocumentReportListadoDocsObsoletosInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wFDocumentReportListadoDocsObsoletosInput.header_authName, wFDocumentReportListadoDocsObsoletosInput.header_authType + wFDocumentReportListadoDocsObsoletosInput.access_token);

            var client = new RestClient(wFDocumentReportListadoDocsObsoletosInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<ReportListadoDocsObsoletos>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<ReportListadoDocsObsoletos> { new ReportListadoDocsObsoletos(result.status + "|" + result.message) };
        }

        public static async Task<List<Departamento>> GetDepartmentsAsync(DepartmentInput departmentsInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(departmentsInput.header_authName, departmentsInput.header_authType + departmentsInput.access_token);

            var client = new RestClient(departmentsInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<Departamento>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<Departamento> { new Departamento(result.status + "|" + result.message) };
        }

        public static async Task<List<TipologiaDocumental>> GetDocumentaryTypologiesAsync(DocumentaryTypologyInput documentaryTypologyInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(documentaryTypologyInput.header_authName, documentaryTypologyInput.header_authType + documentaryTypologyInput.access_token);

            var client = new RestClient(documentaryTypologyInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<TipologiaDocumental>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<TipologiaDocumental> { new TipologiaDocumental(result.status + "|" + result.message) };
        }

        public static async Task<List<RazonesCreacion>> GetCreationReasonsAsync(CreationReasonInput creationReasonInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(creationReasonInput.header_authName, creationReasonInput.header_authType + creationReasonInput.access_token);

            var client = new RestClient(creationReasonInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<RazonesCreacion>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<RazonesCreacion> { new RazonesCreacion(result.status + "|" + result.message) };
        }

        public static async Task<List<TiempoRetencion>> GetRetentionTimesAsync(RetentionTimeInput retentionTimeInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(retentionTimeInput.header_authName, retentionTimeInput.header_authType + retentionTimeInput.access_token);

            var client = new RestClient(retentionTimeInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<TiempoRetencion>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<TiempoRetencion> { new TiempoRetencion(result.status + "|" + result.message) };
        }

        public static async Task<List<MedioAlmacenamiento>> GetStoragesMediaAsync(StorageMediaInput storageMediaInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(storageMediaInput.header_authName, storageMediaInput.header_authType + storageMediaInput.access_token);

            var client = new RestClient(storageMediaInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<MedioAlmacenamiento>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<MedioAlmacenamiento> { new MedioAlmacenamiento(result.status + "|" + result.message) };
        }

        public static async Task<List<FrecuenciaRevision>> GetReviewFrecuenciesAsync(ReviewFrequencyInput reviewFrequencyInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(reviewFrequencyInput.header_authName, reviewFrequencyInput.header_authType + reviewFrequencyInput.access_token);

            var client = new RestClient(reviewFrequencyInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<FrecuenciaRevision>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<FrecuenciaRevision> { new FrecuenciaRevision(result.status + "|" + result.message) };
        }

        public static async Task<List<Clasificacion>> GetClasificationsAsync(ClassificationInput classificationInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(classificationInput.header_authName, classificationInput.header_authType + classificationInput.access_token);

            var client = new RestClient(classificationInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<List<Clasificacion>>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new List<Clasificacion> { new Clasificacion(result.status + "|" + result.message) };
        }

        public static async Task<CrearDocumento> GetDocumentCreateAsync(WFDocumentSelectInput wfDocumentSelectInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wfDocumentSelectInput.header_authName, wfDocumentSelectInput.header_authType + wfDocumentSelectInput.access_token);

            var client = new RestClient(wfDocumentSelectInput.url + "?" + wfDocumentSelectInput.url_paramKey1 + "=" + wfDocumentSelectInput.url_paramValue1 + "&" + wfDocumentSelectInput.url_paramKey2 + "=" + wfDocumentSelectInput.url_paramValue2);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<CrearDocumento>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new CrearDocumento(result.status + "|" + result.message);
        }

        public static async Task<TableroEstadisticas> GetStatisticsBoardAsync(WFStatisticsInput wfStatisticsInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(wfStatisticsInput.header_authName, wfStatisticsInput.header_authType + wfStatisticsInput.access_token);

            var client = new RestClient(wfStatisticsInput.url);

            IRestResponse response = client.Execute(request);

            if (response.IsSuccessful)
                return JsonConvert.DeserializeObject<TableroEstadisticas>(response.Content);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            return new TableroEstadisticas(result.status + "|" + result.message);
        }
        #endregion

        #region "POST_METHODs"
        public static async Task<string> PostDocumentManagerAsync(WFDocumentSaveApproveRejectInput wfDocumentSaveApproveRejectInput, string strJson)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.POST);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_authName, wfDocumentSaveApproveRejectInput.header_authType + wfDocumentSaveApproveRejectInput.access_token);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_key, wfDocumentSaveApproveRejectInput.header_value);

            request.AddParameter(wfDocumentSaveApproveRejectInput.header_value, strJson, ParameterType.RequestBody);

            var client = new RestClient(wfDocumentSaveApproveRejectInput.url);
                        
            IRestResponse response = client.Execute(request);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            if (response.IsSuccessful)
                return response.StatusCode.ToString();

            return result.status + "|" + result.message;
        }
        
        public static async Task<string> PostDocumentCreateAsync(WFDocumentCreateInput wfDocumentCreateInput, CrearDocumento crearDocumento)
        {
            await Task.Delay(5);

            var _json = JsonConvert.SerializeObject(crearDocumento);

            var request = new RestRequest(Method.POST);

            request.AddHeader(wfDocumentCreateInput.header_authName, wfDocumentCreateInput.header_authType + wfDocumentCreateInput.access_token);

            request.AddHeader(wfDocumentCreateInput.header_key, wfDocumentCreateInput.header_value);

            request.AddParameter(wfDocumentCreateInput.header_value, _json, ParameterType.RequestBody);

            var client = new RestClient(wfDocumentCreateInput.url);

            IRestResponse response = client.Execute(request);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            if (response.IsSuccessful)
                return response.StatusCode.ToString();

            return result.status + "|" + result.message;
        }

        public static async Task<string> PostDocumentSaveApproveRejectAsync(WFDocumentSaveApproveRejectInput wfDocumentSaveApproveRejectInput, string strJson)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.POST);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_authName, wfDocumentSaveApproveRejectInput.header_authType + wfDocumentSaveApproveRejectInput.access_token);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_key, wfDocumentSaveApproveRejectInput.header_value);

            request.AddParameter(wfDocumentSaveApproveRejectInput.header_value, strJson, ParameterType.RequestBody);

            var client = new RestClient(wfDocumentSaveApproveRejectInput.url + "?" + wfDocumentSaveApproveRejectInput.url_paramKey1 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue1);

            IRestResponse response = client.Execute(request);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            if (response.IsSuccessful)
                return response.StatusCode.ToString();

            return result.status + "|" + result.message;
        }

        public static async Task<string> PostDocumentAnnexedAsync(WFDocumentAnnexedInput wfDocumentAnnexedInput, string strJson)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.POST);

            request.AddHeader(wfDocumentAnnexedInput.header_authName, wfDocumentAnnexedInput.header_authType + wfDocumentAnnexedInput.access_token);

            request.AddHeader(wfDocumentAnnexedInput.header_key, wfDocumentAnnexedInput.header_value);

            request.AddParameter(wfDocumentAnnexedInput.header_value, strJson, ParameterType.RequestBody);

            var client = new RestClient(wfDocumentAnnexedInput.url);

            IRestResponse response = client.Execute(request);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            if (response.IsSuccessful)
                return response.StatusCode.ToString();

            return result.status + "|" + result.message;
        }
        #endregion

        #region "PUT_METHODs"
        public static async Task<string> PutDocumentUpdateAsync(WFDocumentSaveApproveRejectInput wfDocumentSaveApproveRejectInput, string strJson)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.PUT);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_authName, wfDocumentSaveApproveRejectInput.header_authType + wfDocumentSaveApproveRejectInput.access_token);

            request.AddHeader(wfDocumentSaveApproveRejectInput.header_key, wfDocumentSaveApproveRejectInput.header_value);

            request.AddParameter(wfDocumentSaveApproveRejectInput.header_value, strJson, ParameterType.RequestBody);

            var client = new RestClient(wfDocumentSaveApproveRejectInput.url 
                                        + "?" + wfDocumentSaveApproveRejectInput.url_paramKey1 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue1
                                        + "&" + wfDocumentSaveApproveRejectInput.url_paramKey2 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue2
                                        + "&" + wfDocumentSaveApproveRejectInput.url_paramKey3 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue3
                                        + "&" + wfDocumentSaveApproveRejectInput.url_paramKey4 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue4
                                        + "&" + wfDocumentSaveApproveRejectInput.url_paramKey5 + "=" + wfDocumentSaveApproveRejectInput.url_paramValue5
                                        );

            IRestResponse response = client.Execute(request);

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            if (response.IsSuccessful)
                return response.StatusCode.ToString();

            return result.status + "|" + result.message;
        }
        #endregion
    }
}