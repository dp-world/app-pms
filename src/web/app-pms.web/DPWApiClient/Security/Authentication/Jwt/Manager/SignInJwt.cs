﻿using System;
using System.Text;
using Newtonsoft.Json;
using RestSharp;
using System.Threading.Tasks;
using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Manager
{
    public static class SignInJwt
    {
        public static async Task<TokenOutputJwt> PasswordSignInJwtAsync(TokenInputJwt tokenInputJwt, bool isPersistent, bool lockoutOnFailure)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.POST);

            request.AddHeader(tokenInputJwt.header_authName, tokenInputJwt.header_authType + Convert.ToBase64String(Encoding.Default.GetBytes($"{tokenInputJwt.header_usr}:{tokenInputJwt.header_pwd}")));

            request.AddParameter(tokenInputJwt.header_ContentType, $"{tokenInputJwt.body_key1}={tokenInputJwt.body_val1}&{tokenInputJwt.body_key2}={tokenInputJwt.body_val2}&{tokenInputJwt.body_key3}={tokenInputJwt.body_val3}", ParameterType.RequestBody);

            var client = new RestClient(tokenInputJwt.url);

            IRestResponse response = client.Execute(request); 

            TokenOutputJwt result = JsonConvert.DeserializeObject<TokenOutputJwt>(response.Content) ?? new TokenOutputJwt();

            result.StatusCode = response.StatusCode.ToString();

            result.StatusDescription = string.IsNullOrEmpty(response.StatusDescription) ? string.Empty : response.StatusDescription;

            result.ErrorException = response.ErrorException == null ? string.Empty : response.ErrorException.ToString();

            result.ErrorMessage = string.IsNullOrEmpty(response.ErrorMessage) ? string.Empty : response.ErrorMessage;

            return result;
        }

        public static async Task<UserInfoJwt> GetUserInfoAsync(TokenOutputJwt tokenOutputJwt, UserInfoInput userInfoInput)
        {
            await Task.Delay(5);

            var request = new RestRequest(Method.GET);

            request.AddHeader(userInfoInput.header_authName, userInfoInput.header_authType + tokenOutputJwt.access_token);

            var client = new RestClient(userInfoInput.url);

            IRestResponse response = client.Execute(request);

            var result = JsonConvert.DeserializeObject<UserInfoJwt>(response.Content);

            return result;
        }
    }
}