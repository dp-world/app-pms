﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public class UserInfoInput
    {
        public string url { get; set; }
        public string header_authType { get; set; }
        public string header_authName { get; set; }
    }
}