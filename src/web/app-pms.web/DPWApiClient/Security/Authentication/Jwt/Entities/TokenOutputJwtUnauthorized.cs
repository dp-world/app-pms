﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public partial class TokenOutputJwt
    {
        public string timestamp { get; set; }
        public string status { get; set; }        
        public string message { get; set; }
        public string path { get; set; }
    }
}