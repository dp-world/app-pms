﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public class TokenInputJwt
    {
        public string url { get; set; }
        public string header_usr { get; set; }
        public string header_pwd { get; set; }
        public string header_authName { get; set; }
        public string header_authType { get; set; }
        public string header_ContentType { get; set; }
        public string body_key1 { get; set; }
        public string body_val1 { get; set; }
        public string body_key2 { get; set; }
        public string body_val2 { get; set; }
        public string body_key3 { get; set; }
        public string body_val3 { get; set; }
        public string body_key4 { get; set; }
        public bool body_val4 { get; set; }
    }
}