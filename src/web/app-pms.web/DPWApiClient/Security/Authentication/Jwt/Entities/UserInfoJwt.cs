﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public class UserInfoJwt
    {
        [JsonProperty(PropertyName = "nombreCompleto")]
        public string NombreCompleto { get; set; }

        [JsonProperty(PropertyName = "departamento")]
        public string Departamento { get; set; }

        [JsonProperty(PropertyName = "correoElectronico")]
        public string CorreoElectronico { get; set; }

        public string NombreUsuario { get; set; }
    }
}