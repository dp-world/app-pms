﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public partial class TokenOutputJwt
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }
}