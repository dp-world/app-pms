﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities
{
    public partial class TokenOutputJwt
    {
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
        public string ErrorException { get; set; }
        public string ErrorMessage { get; set; }
    }
}