#pragma checksum "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "064b99c7adb883e1f45f668f998813e63cd7f853"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\_ViewImports.cshtml"
using App_Pms.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\_ViewImports.cshtml"
using App_Pms.Web.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"064b99c7adb883e1f45f668f998813e63cd7f853", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f31624377f0030cf848f1514a553a48f501dc36d", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<App_Pms.Web.DPWApiClient.Workflow.Entities.ListadoBandejaTrabajo>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Document", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Create", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("small-box-footer"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-outline-success btn-sm"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("button"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_6 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("role", new global::Microsoft.AspNetCore.Html.HtmlString("form"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 2 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"<!-- Content Wrapper. Contains page content -->
<div class=""content-wrapper"">
    <!-- Content Header (Page header) -->
    <section class=""content-header"">
        <h1>
            Tablero de Control
            <small>Panel</small>
        </h1>
        <ol class=""breadcrumb"">
            <li><a href=""#""><i class=""fa fa-dashboard""></i> Inicio</a></li>
            <li class=""active"">Tablero de Control</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class=""content"">
        <!-- Alerts -->
");
#nullable restore
#line 21 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
          
            if (ViewData["WFDocument"] != null)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                <div class=""alert alert-success alert-dismissible"">
                    <button type=""button"" class=""close"" data-dismiss=""alert"" aria-hidden=""true"">&times;</button>
                    <h4><i class=""icon fa fa-check""></i> Alerta!</h4>
                    ");
#nullable restore
#line 27 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
               Write(ViewData["WFDocument"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n");
#nullable restore
#line 29 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
            }
        

#line default
#line hidden
#nullable disable
            WriteLiteral(@"        <!-- /.Alerts -->
        <!-- Small boxes (Stat box) -->
        <div class=""row"">
            <div class=""col-lg-3 col-xs-6"">
                <!-- small box -->
                <div class=""small-box bg-aqua"">
                    <div class=""inner"">
                        <h3>150</h3>

                        <p>Creación de Documentos</p>
                    </div>
                    <div class=""icon"">
                        <i class=""ion ion-bag""></i>
                    </div>
                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "064b99c7adb883e1f45f668f998813e63cd7f8538348", async() => {
                WriteLiteral("Flujo Creación <i class=\"fa fa-arrow-circle-right\"></i>");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            BeginWriteTagHelperAttribute();
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __tagHelperExecutionContext.AddHtmlAttribute("a", Html.Raw(__tagHelperStringValueBuffer), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.Minimized);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                </div>
            </div>
            <!-- ./col -->
            <div class=""col-lg-3 col-xs-6"">
                <!-- small box -->
                <div class=""small-box bg-green"">
                    <div class=""inner"">
                        <h3>53<sup style=""font-size: 20px"">%</sup></h3>

                        <p>Modificación de Documentos</p>
                    </div>
                    <div class=""icon"">
                        <i class=""ion ion-stats-bars""></i>
                    </div>
                    <a href=""#"" class=""small-box-footer"">Mas Info <i class=""fa fa-arrow-circle-right""></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class=""col-lg-3 col-xs-6"">
                <!-- small box -->
                <div class=""small-box bg-yellow"">
                    <div class=""inner"">
                        <h3>44</h3>

                        <p>Obsoletización de Documentos</p>
                    </div>
   ");
            WriteLiteral(@"                 <div class=""icon"">
                        <i class=""ion ion-person-add""></i>
                    </div>
                    <a href=""#"" class=""small-box-footer"">Mas Info <i class=""fa fa-arrow-circle-right""></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class=""col-lg-3 col-xs-6"">
                <!-- small box -->
                <div class=""small-box bg-red"">
                    <div class=""inner"">
                        <h3>65</h3>

                        <p>Reactivación de Documentos</p>
                    </div>
                    <div class=""icon"">
                        <i class=""ion ion-pie-graph""></i>
                    </div>
                    <a href=""#"" class=""small-box-footer"">Mas Info <i class=""fa fa-arrow-circle-right""></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class=""row"">
            <!");
            WriteLiteral(@"-- section Left col -->
            <section class=""col-lg-12 connectedSortable"">
                <!-- col -->
                <div class=""col-xs-12"">
                    <!-- box -->
                    <div class=""box"">
                        <div class=""box-header"">
                            <h3 class=""box-title"">Cola de Trabajo - Documentos</h3>
                        </div>
                        <!-- datatable start -->
                        <div class=""box-body"">
                            <table id=""example1"" class=""table table-bordered table-hover"">
                                <thead>
                                    <tr>
");
            WriteLiteral(@"                                        <th>Tipo Solicitud</th>
                                        <th>Departamento</th>
                                        <th>Fecha</th>
                                        <th>Nombre Paso</th>
                                        <th>Requiere Soporte</th>
                                        <th>Nombre Documento</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
");
#nullable restore
#line 126 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                     foreach (var item in Model)
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                        <tr>\r\n");
            WriteLiteral("                                            <td>\r\n                                                ");
#nullable restore
#line 138 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.TipoSolicitud));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
#nullable restore
#line 141 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.Departamento));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
#nullable restore
#line 144 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.Fecha));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
#nullable restore
#line 147 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.NombrePaso));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
#nullable restore
#line 150 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.RequiereSoporte));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
#nullable restore
#line 153 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                           Write(Html.DisplayFor(modelItem => item.NombreDocumento));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                                            </td>\r\n                                            <td>\r\n                                                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "064b99c7adb883e1f45f668f998813e63cd7f85317029", async() => {
                WriteLiteral("Edit");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_5.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_5);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 156 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                                      WriteLiteral(item.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 156 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                                                                  WriteLiteral(item.IdPaso);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idPaso"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-idPaso", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["idPaso"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 156 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                                                                                                    WriteLiteral(item.DocName);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dDocName"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-dDocName", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dDocName"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 156 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                                                                                                                                        WriteLiteral(item.NumeroRevisionDocumento);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dRevLabel"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-dRevLabel", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dRevLabel"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 156 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                                                                                                                                                                                            WriteLiteral(item.GrupoSeguridad);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dSecGroup"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-dSecGroup", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["dSecGroup"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                                <button id=\"btnEdit\" type=\"button\" class=\"btn btn-info\" data-valueId=\"");
#nullable restore
#line 157 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                 Write(item.Id);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" data-valueIdPaso=\"");
#nullable restore
#line 157 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                                                                                                                             Write(item.IdPaso);

#line default
#line hidden
#nullable disable
            WriteLiteral("\" data-toggle=\"modal\" data-target=\"#modal-default\"><i class=\"fa fa-edit\"></i></button>\r\n");
            WriteLiteral("                                            </td>\r\n                                        </tr>\r\n");
#nullable restore
#line 161 "C:\Development\DotNet\Workspace\Sietipro\MSVS\CS\Core 3.0\DP World\app-pms\src\web\app-pms.web\Views\Home\Index.cshtml"
                                    }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                </tbody>
                            </table>
                        </div>
                        <!-- datatable end -->
                        <!-- modal start -->
                        <div class=""modal fade"" id=""modal-default"">
                            <!-- modal-dialog -->
                            <div class=""modal-dialog"">
                                <!-- modal-content -->
                                <div class=""modal-content"">
                                    <div class=""modal-header"">
                                        <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                                            <span aria-hidden=""true"">&times;</span>
                                        </button>
                                        <h4 class=""modal-title"">Editar Documentos / []</h4>
                                    </div>
                                    <div class=""modal-body"">
        ");
            WriteLiteral("                                <!-- form start -->\r\n                                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("form", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "064b99c7adb883e1f45f668f998813e63cd7f85326123", async() => {
                WriteLiteral(@"
                                            <!--box-body -->
                                            <div class=""box-body"">
                                                <div id=""ContentModalEdit"">
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                        ");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.RenderAtEndOfFormTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_RenderAtEndOfFormTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_6);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                                        <!-- /form start -->
                                    </div>
                                    <div class=""modal-footer"">
                                        <button type=""button"" class=""btn btn-default pull-left"" data-dismiss=""modal"">Cerrar</button>
                                        <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Aprobar</button>
                                        <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Descargar</button>
                                        <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">Rechazar</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal end -->

                    </div>
 ");
            WriteLiteral(@"                   <!-- /.box -->
                </div>
                <!-- /.col -->
            </section>
            <!-- /.section Left col -->

        </div>
        <!-- /.row (Main row) -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type=""text/javascript"">
    $(document).ready(function () {
        $(""#btnEdit"").click(function () {
            var id = $(this).data(""valueId"");
            var idPaso = $(this).data(""valueIdPaso"");
            $(""#ContentModalEdit"").load(""/Document/Edit/"" + id + ""?idPaso="" + idPaso,
                function () {
                    $(""#modal-default"").modal(""show"");
                }
            );
        });
    });
</script>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<App_Pms.Web.DPWApiClient.Workflow.Entities.ListadoBandejaTrabajo>> Html { get; private set; }
    }
}
#pragma warning restore 1591
