﻿using Microsoft.AspNetCore.Http;

namespace App_Pms.Web.Security
{
    public static class SessionState
    {
        private static IHttpContextAccessor _httpContextAccessor;

        public static void Configure(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public static HttpContext Current => _httpContextAccessor.HttpContext;

    }
}