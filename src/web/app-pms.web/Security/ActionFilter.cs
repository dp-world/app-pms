﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace App_Pms.Web.Security
{
    public class SessionTimeoutAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context){     
            
            if (string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token"))) {

                context.Result = new RedirectToActionResult("Login", "Account", null);

                return;
            }
            base.OnActionExecuting(context);
        }
    }
}