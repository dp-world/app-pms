﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App_Pms.Web.Security;
using System.IO;
using Microsoft.AspNetCore.Mvc.Rendering;
using App_Pms.Web.DPWApiClient.Workflow.Entities;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using Microsoft.Extensions.Options;
using App_Pms.Web.DPWApiClient.Workflow.Manager;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Authorization;

namespace App_Pms.Web.Controllers
{
    [Authorize]
    public class DocumentController : Controller
    {
        private readonly IPInput _ipInput;
        private readonly DepartmentInput _departmentsInput;
        private readonly DocumentaryTypologyInput _documentaryTypologyInput;
        private readonly CreationReasonInput _creationReasonInput;
        private readonly RetentionTimeInput _retentionTimeInput;
        private readonly StorageMediaInput _storageMediaInput;
        private readonly ReviewFrequencyInput _reviewFrequencyInput;
        private readonly ClassificationInput _classificationInput;
        private readonly WFDocumentCreateInput _wfDocumentCreateInput;
        private readonly WFDocumentSelectInput _wfDocumentSelectInput;
        private readonly WFDocumentSelectListDocsInput _wfDocumentSelectListDocsInput;
        private readonly WFDocumentSaveApproveRejectInput _wfDocumentSaveApproveRejectInput;
        private readonly WFDocumentDownloadInput _wfDocumentDownloadInput;
        private readonly WFDocumentTemplatesInput _wfDocumentTemplatesInput;
        private readonly WFDocumentAnnexedInput _wfDocumentAnnexedInput;
        private readonly WFDocumentReportMatrizRetencionesInput _wfDocumentReportMatrizRetencionesInput;
        private readonly WFDocumentReportListadoMaestroDocsInput _wfDocumentReportListadoMaestroDocsInput;
        private readonly WFDocumentReportListadoDocsObsoletosInput _wfDocumentReportListadoDocsObsoletosInput;
        private readonly LastStepInput _lastStepInput;
        private readonly string _strIP;

        public DocumentController(
                                  IOptions<IPInput> ipInput,
                                  IOptions<DepartmentInput> departmentsInput,
                                  IOptions<DocumentaryTypologyInput> documentaryTypologyInput,
                                  IOptions<CreationReasonInput> creationReasonInput,
                                  IOptions<RetentionTimeInput> retentionTimeInput,
                                  IOptions<StorageMediaInput> storageMediaInput,
                                  IOptions<ReviewFrequencyInput> reviewFrequencyInput,
                                  IOptions<ClassificationInput> classificationInput,
                                  IOptions<WFDocumentCreateInput> wfDocumentCreateInput,
                                  IOptions<WFDocumentSelectInput> wfDocumentSelectInput,
                                  IOptions<WFDocumentSelectListDocsInput> wfDocumentSelectListDocsInput,
                                  IOptions<WFDocumentSaveApproveRejectInput> wfDocumentSaveApproveRejectInput,
                                  IOptions<WFDocumentDownloadInput> wfDocumentDownloadInput,
                                  IOptions<WFDocumentTemplatesInput> wfDocumentTemplatesInput,
                                  IOptions<WFDocumentAnnexedInput> wfDocumentAnnexedInput,
                                  IOptions<WFDocumentReportMatrizRetencionesInput> wfDocumentReportMatrizRetencionesInput,
                                  IOptions<WFDocumentReportListadoMaestroDocsInput> wfDocumentReportListadoMaestroDocsInput,
                                  IOptions<WFDocumentReportListadoDocsObsoletosInput> wfDocumentReportListadoDocsObsoletosInput,
                                  IOptions<LastStepInput> lastStepInput
                                 )
        {
            _ipInput = ipInput.Value;
            _departmentsInput = departmentsInput.Value;
            _documentaryTypologyInput = documentaryTypologyInput.Value;
            _creationReasonInput = creationReasonInput.Value;
            _retentionTimeInput = retentionTimeInput.Value;
            _storageMediaInput = storageMediaInput.Value;
            _reviewFrequencyInput = reviewFrequencyInput.Value;
            _classificationInput = classificationInput.Value;
            _wfDocumentCreateInput = wfDocumentCreateInput.Value;
            _wfDocumentSelectInput = wfDocumentSelectInput.Value;
            _wfDocumentSelectListDocsInput = wfDocumentSelectListDocsInput.Value;
            _wfDocumentSaveApproveRejectInput = wfDocumentSaveApproveRejectInput.Value;
            _wfDocumentDownloadInput = wfDocumentDownloadInput.Value;
            _wfDocumentTemplatesInput = wfDocumentTemplatesInput.Value;
            _wfDocumentAnnexedInput = wfDocumentAnnexedInput.Value;
            _wfDocumentReportMatrizRetencionesInput = wfDocumentReportMatrizRetencionesInput.Value;
            _wfDocumentReportListadoMaestroDocsInput = wfDocumentReportListadoMaestroDocsInput.Value;
            _wfDocumentReportListadoDocsObsoletosInput = wfDocumentReportListadoDocsObsoletosInput.Value;
            _lastStepInput = lastStepInput.Value;

            _strIP = _ipInput.flag_url_dpw.Equals("0") ? _ipInput.url_dev : (_ipInput.flag_url_dpw.Equals("1") ? _ipInput.url_prd : string.Empty);
            
            _departmentsInput.url = _departmentsInput.url.Replace("{IP}", _strIP);
            _documentaryTypologyInput.url = _documentaryTypologyInput.url.Replace("{IP}", _strIP);
            _creationReasonInput.url = _creationReasonInput.url.Replace("{IP}", _strIP);
            _retentionTimeInput.url = _retentionTimeInput.url.Replace("{IP}", _strIP);
            _storageMediaInput.url = _storageMediaInput.url.Replace("{IP}", _strIP);
            _reviewFrequencyInput.url = _reviewFrequencyInput.url.Replace("{IP}", _strIP);
            _classificationInput.url = _classificationInput.url.Replace("{IP}", _strIP);
            _wfDocumentCreateInput.url = _wfDocumentCreateInput.url.Replace("{IP}", _strIP);
            _wfDocumentSelectInput.url = _wfDocumentSelectInput.url.Replace("{IP}", _strIP);
            _wfDocumentSelectListDocsInput.url = _wfDocumentSelectListDocsInput.url.Replace("{IP}", _strIP);
            _wfDocumentSaveApproveRejectInput.url = _wfDocumentSaveApproveRejectInput.url.Replace("{IP}", _strIP);
            _wfDocumentDownloadInput.url = _wfDocumentDownloadInput.url.Replace("{IP}", _strIP);
            _wfDocumentTemplatesInput.url = _wfDocumentTemplatesInput.url.Replace("{IP}", _strIP);
            _wfDocumentAnnexedInput.url = _wfDocumentAnnexedInput.url.Replace("{IP}", _strIP);
            _wfDocumentReportMatrizRetencionesInput.url = _wfDocumentReportMatrizRetencionesInput.url.Replace("{IP}", _strIP);
            _wfDocumentReportListadoMaestroDocsInput.url = _wfDocumentReportListadoMaestroDocsInput.url.Replace("{IP}", _strIP);
            _wfDocumentReportListadoDocsObsoletosInput.url = _wfDocumentReportListadoDocsObsoletosInput.url.Replace("{IP}", _strIP);
        }

        // GET: Document
        [SessionTimeout]
        public async Task<IActionResult> Index()
        {
            try
            {
                var _listadoDocs = await ListDocsQueueAsync();
                ViewData["url_download"] = _wfDocumentDownloadInput.url;

                if (_listadoDocs.Count > 0) 
                {
                    if (string.IsNullOrEmpty(_listadoDocs[0].result))
                        return View(_listadoDocs);

                    return RedirectToAction("Error", "Home", new { @statusCode = _listadoDocs[0].result.Split("|")[0], @message = _listadoDocs[0].result.Split("|")[1] });
                }
                return View(_listadoDocs);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // GET: Document/Create
        [SessionTimeout]
        public async Task<ActionResult> Create([FromQuery]string TipoSolicitud)
        {
            try
            {
                CrearDocumento crearDocumento = new CrearDocumento();

                ViewData["url_api_searchDocName"] = _ipInput.url_api_searchDocName;

                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("full_name")))
                    crearDocumento.Solicitante = SessionState.Current.Session.GetString("full_name");

                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                    ViewData["jwt_access_token"] = SessionState.Current.Session.GetString("access_token");

                ViewData["uri_buscar_docsXtitulo"] = _wfDocumentSelectListDocsInput.url;

                // Departments {
                var _departamentos = await DepartmentsAsync();
                if (!string.IsNullOrEmpty(_departamentos[0].result))
                    return RedirectToAction("Error", "Home", new { @statusCode = _departamentos[0].result.Split("|")[0], @message = _departamentos[0].result.Split("|")[1] });

                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("department")))
                {
                    ViewData["IdDepartamento"] = new SelectList(_departamentos, "CodigoDepartamento", "NombreDepartamento", SessionState.Current.Session.GetString("department"));
                }
                else
                {
                    ViewData["IdDepartamento"] = new SelectList(_departamentos, "CodigoDepartamento", "NombreDepartamento");
                }
                // ./Departments

                // DocumentaryTypologies {
                var _tipologias = await DocumentaryTypologiesAsync();
                if (!string.IsNullOrEmpty(_tipologias[0].result))
                    return RedirectToAction("Error", "Home", new { @statusCode = _tipologias[0].result.Split("|")[0], @message = _tipologias[0].result.Split("|")[1] });
                
                ViewData["IdTipologia"] = new SelectList(_tipologias, "CodigoTipoDocumental", "NombreTipoDocumental");
                // ./DocumentaryTypologies

                // CreationReasons {
                var _razonesCrea = await CreationReasonsAsync();
                if (!string.IsNullOrEmpty(_razonesCrea[0].result))
                    return RedirectToAction("Error", "Home", new { @statusCode = _razonesCrea[0].result.Split("|")[0], @message = _razonesCrea[0].result.Split("|")[1] });

                ViewData["IdRazonCreacion"] = new SelectList(_razonesCrea, "CodigoRazonCreacion", "NombreRazonCreacion");
                // ./CreationReasons

                ViewData["IdTipoCambio"] = new SelectList(GetTiposCambios(), "codigo", "nombreTipoCambio");
                ViewData["TipoSolicitud"] = TipoSolicitud; ViewData["TipoSolicitudLabel"] = TipoSolicitud.Replace("cion", "ción").Replace("Nomenclatura", "Cambio de Nomenclatura");
                ViewData["url_templates"] = _wfDocumentTemplatesInput.url;
                crearDocumento.TipoSolicitud = TipoSolicitud;

                return View(crearDocumento);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // POST: Document/Create
        [HttpPost]
        [SessionTimeout]
        public async Task<ActionResult> Create(CrearDocumento crearDocumento)
        {
            try
            {
                crearDocumento.TituloDocumento = "DOCUMENTO TEMPORAL.TXT";
                crearDocumento.DocumentoBase64 = "RkFWT1IgRUxJTUlOQVIgRVNUQSBMw41ORUEgREUgQ8OTRElHTyBVTkEgVkVaIFNFIEFKVVNURSBMQSBPQkxJR0FUT1JJRURBRCBERSBBREpVTlRBUiBET0NVTUVOVE8gRU4gTE9TIEZMVUpPUyBERSBBQ1RVQUxJWkFSIFkgT0JTT0xFVElaQVI=";
                // TODO: Add insert logic here
                if (ModelState.IsValid)
                {

                    if (crearDocumento.FormFile != null)
                    {
                        crearDocumento.TituloDocumento = crearDocumento.FormFile.FileName;
                        using (var memoryStream = new MemoryStream())
                        {
                            await crearDocumento.FormFile.CopyToAsync(memoryStream);
                            var fileBytes = memoryStream.ToArray();
                            crearDocumento.DocumentoBase64 = Convert.ToBase64String(fileBytes);
                        }
                        if (crearDocumento.FormFileAnnexed != null)
                        {
                            crearDocumento.TituloDocumentoAnexo = crearDocumento.FormFileAnnexed.FileName;
                            using (var memoryStream = new MemoryStream())
                            {
                                await crearDocumento.FormFileAnnexed.CopyToAsync(memoryStream);
                                var fileBytes = memoryStream.ToArray();
                                crearDocumento.documentoAnexo = Convert.ToBase64String(fileBytes);
                            }
                        }
                    }

                    AnexarDocumento anexarDocumento = new AnexarDocumento();
                    if (crearDocumento.FormFileAnnexed != null)
                    {
                        anexarDocumento.TituloDocumento = crearDocumento.FormFileAnnexed.FileName;
                        using (var memoryStream = new MemoryStream())
                        {
                            await crearDocumento.FormFileAnnexed.CopyToAsync(memoryStream);
                            var fileBytes = memoryStream.ToArray();
                            anexarDocumento.DocumentoBase64 = Convert.ToBase64String(fileBytes);
                            anexarDocumento.IdDocPrincipal = crearDocumento.Id.ToString();
                        }
                    }

                    var result = await WFDocumentCreateAsync(crearDocumento);
                    //var result = await WFDocumentManagerAsync(JsonConvert.SerializeObject(crearDocumento), true);
                    if (result.Equals("OK"))
                    {
                        var msg = "El documento se ha creado exitosamente";
                        if (!crearDocumento.TipoSolicitud.Equals("Creacion"))
                        {
                            if (crearDocumento.FormFileAnnexed != null)
                            {
                                result = await WFDocumentAnnexedAsync(JsonConvert.SerializeObject(anexarDocumento));
                                if (result.Equals("OK"))
                                {
                                    msg += "  Adicional, el documento anexo se adjuntó de forma exitosa. ";
                                }
                                else
                                {
                                    msg += "  Adicional, el documento anexo NO se logró guardar. ";
                                }
                            }
                        }
                        HttpContext.Session.SetString("WFDocument", msg);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        HttpContext.Session.SetString("WFDocument", "Error");
                        return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
                    }
                }
                return View(crearDocumento);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // GET: Document/Edit/5
        [SessionTimeout]
        public async Task<ActionResult> Edit(int? id, [FromQuery]int? idPaso, [FromQuery]string dDocName, [FromQuery]string dRevLabel, [FromQuery]string dSecGroup, [FromQuery]string NombrePaso)
        {
            try
            {
                ViewData["url_api_searchDocName"] = _ipInput.url_api_searchDocName;

                if (id == null || idPaso == null)
                    return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = "Documento Nulo" });

                // GetDocumentCreate {
                var wfDocumentoCreado = await GetWFDocumentCreateAsync(idPaso.ToString(), id.ToString());

                if (wfDocumentoCreado == null)
                    return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = "Documento Errado" });

                if (!string.IsNullOrEmpty(wfDocumentoCreado.result))
                    return RedirectToAction("Error", "Home", new { @statusCode = wfDocumentoCreado.result.Split("|")[0], @message = wfDocumentoCreado.result.Split("|")[1] });
                // ./GetDocumentCreate

                // Departments {
                var _departamentos = await DepartmentsAsync();
                if (!string.IsNullOrEmpty(_departamentos[0].result))
                    return RedirectToAction("Error", "Home", new { @statusCode = _departamentos[0].result.Split("|")[0], @message = _departamentos[0].result.Split("|")[1] });

                ViewData["IdDepartamento"] = new SelectList(_departamentos, "CodigoDepartamento", "NombreDepartamento", wfDocumentoCreado.Departamento.CodigoDepartamento);
                // ./Departments

                // DocumentaryTypologies {
                if (wfDocumentoCreado.TipologiaDocumental != null) {                    
                    var _tipologias = await DocumentaryTypologiesAsync();
                    if (!string.IsNullOrEmpty(_tipologias[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _tipologias[0].result.Split("|")[0], @message = _tipologias[0].result.Split("|")[1] });

                    ViewData["IdTipologia"] = new SelectList(_tipologias, "CodigoTipoDocumental", "NombreTipoDocumental", wfDocumentoCreado.TipologiaDocumental.CodigoTipoDocumental);
                }
                // ./DocumentaryTypologies

                // CreationReasons
                if (wfDocumentoCreado.RazonesCreacion != null) 
                {
                    var _razonesCrea = await CreationReasonsAsync();
                    if (!string.IsNullOrEmpty(_razonesCrea[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _razonesCrea[0].result.Split("|")[0], @message = _razonesCrea[0].result.Split("|")[1] });

                    ViewData["IdRazonCreacion"] = new SelectList(_razonesCrea, "CodigoRazonCreacion", "NombreRazonCreacion", wfDocumentoCreado.RazonesCreacion.CodigoRazonCreacion);
                }
                // ./CreationReasons

                // RetentionTimes
                if (_lastStepInput.IDs.Contains(idPaso.ToString()))
                {
                    var _tiemposReten = await RetentionTimesAsync();
                    if (!string.IsNullOrEmpty(_tiemposReten[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _tiemposReten[0].result.Split("|")[0], @message = _tiemposReten[0].result.Split("|")[1] });

                    if (!string.IsNullOrEmpty(wfDocumentoCreado.TiempoRetencion))
                    {
                        ViewData["IdTiempoRetencion"] = new SelectList(_tiemposReten, "CodigoTiempoRetencion", "NombreTiempoRetencion", wfDocumentoCreado.TiempoRetencion);
                    }
                    else 
                    {
                        ViewData["IdTiempoRetencion"] = new SelectList(_tiemposReten, "CodigoTiempoRetencion", "NombreTiempoRetencion");
                    }
                }
                // ./RetentionTimes

                // StoragesMedia
                if (_lastStepInput.IDs.Contains(idPaso.ToString()))
                {
                    var _mediosAlmacena = await StoragesMediaAsync();
                    if (!string.IsNullOrEmpty(_mediosAlmacena[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _mediosAlmacena[0].result.Split("|")[0], @message = _mediosAlmacena[0].result.Split("|")[1] });

                    if (!string.IsNullOrEmpty(wfDocumentoCreado.MedioAlmacenamiento))
                    {
                        ViewData["IdMedioAlmacenamiento"] = new SelectList(_mediosAlmacena, "CodigoMedioAlmacenamiento", "NombreMedioAlmacenamiento", wfDocumentoCreado.MedioAlmacenamiento);
                    }
                    else 
                    {
                        ViewData["IdMedioAlmacenamiento"] = new SelectList(_mediosAlmacena, "CodigoMedioAlmacenamiento", "NombreMedioAlmacenamiento");
                    }
                }
                // ./StoragesMedia

                // ReviewFrecuencies
                if (_lastStepInput.IDs.Contains(idPaso.ToString()))
                {
                    var _frecuenciaRev = await ReviewFrecuenciesAsync();
                    if (!string.IsNullOrEmpty(_frecuenciaRev[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _frecuenciaRev[0].result.Split("|")[0], @message = _frecuenciaRev[0].result.Split("|")[1] });

                    if (!string.IsNullOrEmpty(wfDocumentoCreado.FrecuenciaRevision))
                    {
                        ViewData["IdFrecuenciaRevision"] = new SelectList(_frecuenciaRev, "CodigoFrecuenciaRevision", "NombreFrecuenciaRevision", wfDocumentoCreado.FrecuenciaRevision);
                    }
                    else 
                    {
                        ViewData["IdFrecuenciaRevision"] = new SelectList(_frecuenciaRev, "CodigoFrecuenciaRevision", "NombreFrecuenciaRevision");
                    }
                }
                // ./ReviewFrecuencies

                // Clasifications
                if (_lastStepInput.IDs.Contains(idPaso.ToString()))
                {
                    var _clasificaciones = await ClasificationsAsync();
                    if (!string.IsNullOrEmpty(_clasificaciones[0].result))
                        return RedirectToAction("Error", "Home", new { @statusCode = _clasificaciones[0].result.Split("|")[0], @message = _clasificaciones[0].result.Split("|")[1] });

                    if (!string.IsNullOrEmpty(wfDocumentoCreado.Clasificacion))
                    {
                        ViewData["IdClasificacion"] = new SelectList(_clasificaciones, "CodigoClasificacion", "NombreClasificacion", wfDocumentoCreado.Clasificacion);
                    }
                    else 
                    {
                        ViewData["IdClasificacion"] = new SelectList(_clasificaciones, "CodigoClasificacion", "NombreClasificacion");
                    }
                }
                // ./Clasifications

                if (wfDocumentoCreado.TipoCambio != null)
                    ViewData["IdTipoCambio"] = new SelectList(GetTiposCambios(), "codigo", "nombreTipoCambio", wfDocumentoCreado.TipoCambio);

                ViewData["url_download"] = _wfDocumentDownloadInput.url;

                ViewData["TipoSolicitud"] = wfDocumentoCreado.TipoSolicitud; ViewData["TipoSolicitudLabel"] = wfDocumentoCreado.TipoSolicitud.Replace("cion", "ción").Replace("Nomenclatura", "Cambio de Nomenclatura");

                ViewData["IdPaso"] = idPaso.ToString();

                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                    ViewData["jwt_access_token"] = SessionState.Current.Session.GetString("access_token");

                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("user_name")))
                    ViewData["user_name"] = SessionState.Current.Session.GetString("user_name");

                wfDocumentoCreado.FechaVencimiento = wfDocumentoCreado.FechaVencimiento.Replace(" 00:00:00Z","");
                wfDocumentoCreado.Id = (int)id;
                wfDocumentoCreado.IdPaso = (int)idPaso;
                wfDocumentoCreado.DocName = dDocName;
                wfDocumentoCreado.NumeroRevisionDocumento = dRevLabel;
                wfDocumentoCreado.GrupoSeguridad = dSecGroup;
                wfDocumentoCreado.NombrePaso = NombrePaso;

                return View(wfDocumentoCreado);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // POST: Document/Edit/
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionTimeout]
        public async Task<ActionResult> Edit(CrearDocumento crearDocumento)
        {
            try
            {
                var result = await Update(crearDocumento);

                if (result.Equals("OK"))
                    return RedirectToAction("Index", "Home");
                
                    return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        private async Task<string> Update(CrearDocumento crearDocumento) 
        {
            try
            {
                // TODO: Add Edit/Update logic here
                _wfDocumentSaveApproveRejectInput.url_paramValue1 = crearDocumento.Id.ToString();
                _wfDocumentSaveApproveRejectInput.url_paramValue2 = crearDocumento.DocName;
                _wfDocumentSaveApproveRejectInput.url_paramValue3 = crearDocumento.NumeroRevisionDocumento;
                _wfDocumentSaveApproveRejectInput.url_paramValue4 = crearDocumento.GrupoSeguridad;
                _wfDocumentSaveApproveRejectInput.url_paramValue5 = crearDocumento.IdPaso.ToString();

                if (!string.IsNullOrEmpty(crearDocumento.FechaVencimiento))
                    crearDocumento.FechaVencimiento += " 00:00:00";

                AnexarDocumento anexarDocumento = new AnexarDocumento();
                if (crearDocumento.FormFileAnnexed != null)
                {
                    anexarDocumento.TituloDocumento = crearDocumento.FormFileAnnexed.FileName;
                    using (var memoryStream = new MemoryStream())
                    {
                        await crearDocumento.FormFileAnnexed.CopyToAsync(memoryStream);
                        var fileBytes = memoryStream.ToArray();
                        anexarDocumento.DocumentoBase64 = Convert.ToBase64String(fileBytes);
                        anexarDocumento.IdDocPrincipal = crearDocumento.Id.ToString();
                    }
                }

                if (crearDocumento.FormFile != null)
                {
                    crearDocumento.TituloDocumento = crearDocumento.FormFile.FileName;
                    using (var memoryStream = new MemoryStream())
                    {
                        await crearDocumento.FormFile.CopyToAsync(memoryStream);
                        var fileBytes = memoryStream.ToArray();
                        crearDocumento.DocumentoBase64 = Convert.ToBase64String(fileBytes);
                    }
                }

                var result = await WFDocumentUpdateAsync(JsonConvert.SerializeObject(crearDocumento));
                //var result = await WFDocumentManagerAsync(JsonConvert.SerializeObject(crearDocumento));
                if (result.Equals("OK"))
                {
                    var msg = "El documento se ha actualizado correctamente.";

                    if (crearDocumento.FormFileAnnexed != null)
                    {
                        result = await WFDocumentAnnexedAsync(JsonConvert.SerializeObject(anexarDocumento));
                        if (result.Equals("OK"))
                        {
                            msg += "  Adicional, el documento anexo se adjuntó de forma exitosa. ";
                        }
                        else
                        {
                            msg += "  Adicional, el documento anexo NO se logró guardar. ";
                        }
                    }
                    HttpContext.Session.SetString("WFDocument", msg);
                }
                else
                {
                    HttpContext.Session.SetString("WFDocument", "Error");
                }
                return result;
            }
            catch (Exception ex)
            {
                return ex.Message + " ..::.. " + ex.ToString();
            }
        }

        // POST: Document/Approve/
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionTimeout]
        public async Task<ActionResult> Approve(CrearDocumento crearDocumento)
        {
            try
            {
                var result = await Update(crearDocumento);
                if (result.Equals("OK"))
                {
                    // TODO: Add Approve logic here
                    _wfDocumentSaveApproveRejectInput.url_paramValue1 = crearDocumento.Id.ToString();
                    result = await WFDocumentSaveApproveRejectAsync(JsonConvert.SerializeObject(new AprobarDocumento()));

                    //var result = await WFDocumentManagerAsync(JsonConvert.SerializeObject(new AprobarDocumento()));
                    if (result.Equals("OK"))
                    {
                        var msg = string.Empty;
                        if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("WFDocument")))
                            msg = SessionState.Current.Session.GetString("WFDocument");

                        msg += " Adicional, el documento se ha aprobado de forma correcta. ";

                        HttpContext.Session.SetString("WFDocument", msg);
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        HttpContext.Session.SetString("WFDocument", "Error");
                        return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
                    }
                }
                else
                {
                    HttpContext.Session.SetString("WFDocument", "Error");
                    return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // POST: Document/Reject/
        [HttpPost]
        [ValidateAntiForgeryToken]
        [SessionTimeout]
        public async Task<ActionResult> Reject(CrearDocumento crearDocumento)
        {
            try
            {
                var result = await Update(crearDocumento);
                if (result.Equals("OK"))
                {
                    var _justificacion = string.IsNullOrEmpty(crearDocumento.Justificacion) ? string.Empty : crearDocumento.Justificacion;
                    if (crearDocumento.TipoSolicitud.Equals("Nomenclatura"))
                        _justificacion = crearDocumento.RazonDelCambio;

                    // TODO: Add Reject logic here
                    var rechazarDoc = new RechazarDocumento
                    {
                        RazonRechazo = _justificacion
                    };

                    _wfDocumentSaveApproveRejectInput.url_paramValue1 = crearDocumento.Id.ToString();
                    result = await WFDocumentSaveApproveRejectAsync(JsonConvert.SerializeObject(rechazarDoc));
                    //var result = await WFDocumentManagerAsync(JsonConvert.SerializeObject(rechazarDoc));

                    if (result.Equals("OK"))
                    {
                        HttpContext.Session.SetString("WFDocument", "El documento se ha rechazado de forma correcta.");
                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        HttpContext.Session.SetString("WFDocument", "Error");
                        return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
                    }
                }
                else 
                {
                    HttpContext.Session.SetString("WFDocument", "Error");
                    return RedirectToAction("Error", "Home", new { @statusCode = result.Split("|")[0] , @message = result.Split("|")[1] });
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // GET: ReportMatrizRetenciones
        [SessionTimeout]
        public async Task<IActionResult> ReportMatrizRetenciones()
        {
            try
            {
                var _reportMatrizRetenciones = await MatrizRetencionesAsync();

                if (_reportMatrizRetenciones.Count > 0) 
                {
                    if (string.IsNullOrEmpty(_reportMatrizRetenciones[0].result))
                        return View(_reportMatrizRetenciones);

                    return RedirectToAction("Error", "Home", new { @statusCode = _reportMatrizRetenciones[0].result.Split("|")[0], @message = _reportMatrizRetenciones[0].result.Split("|")[1] });
                }
                return View(_reportMatrizRetenciones);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // GET: ReportListadoMaestroDocs
        [SessionTimeout]
        public async Task<IActionResult> ReportListadoMaestroDocs()
        {
            try
            {
                var _reportListadoMaestroDocs = await ListadoMaestroDocsAsync();

                if (_reportListadoMaestroDocs.Count > 0) 
                {
                    if (string.IsNullOrEmpty(_reportListadoMaestroDocs[0].result))
                        return View(_reportListadoMaestroDocs);

                    return RedirectToAction("Error", "Home", new { @statusCode = _reportListadoMaestroDocs[0].result.Split("|")[0], @message = _reportListadoMaestroDocs[0].result.Split("|")[1] });
                }
                return View(_reportListadoMaestroDocs);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        // GET: ReportListadoDocsObsoletos
        [SessionTimeout]
        public async Task<IActionResult> ReportListadoDocsObsoletos()
        {
            try
            {
                var _reportListadoDocsObsoletos = await ListadoDocsObsoletosAsync();

                if (_reportListadoDocsObsoletos.Count > 0) 
                {
                    if (string.IsNullOrEmpty(_reportListadoDocsObsoletos[0].result))
                        return View(_reportListadoDocsObsoletos);

                    return RedirectToAction("Error", "Home", new { @statusCode = _reportListadoDocsObsoletos[0].result.Split("|")[0], @message = _reportListadoDocsObsoletos[0].result.Split("|")[1] });
                }
                return View(_reportListadoDocsObsoletos);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        private async Task<List<Departamento>> DepartmentsAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _departmentsInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetDepartmentsAsync(_departmentsInput);
        }

        private async Task<List<TipologiaDocumental>> DocumentaryTypologiesAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _documentaryTypologyInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetDocumentaryTypologiesAsync(_documentaryTypologyInput);
        }

        private async Task<List<RazonesCreacion>> CreationReasonsAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _creationReasonInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetCreationReasonsAsync(_creationReasonInput);
        }

        private async Task<List<TiempoRetencion>> RetentionTimesAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _retentionTimeInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetRetentionTimesAsync(_retentionTimeInput);
        }

        private async Task<List<MedioAlmacenamiento>> StoragesMediaAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _storageMediaInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetStoragesMediaAsync(_storageMediaInput);
        }

        private async Task<List<FrecuenciaRevision>> ReviewFrecuenciesAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _reviewFrequencyInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetReviewFrecuenciesAsync(_reviewFrequencyInput);
        }

        private async Task<List<Clasificacion>> ClasificationsAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _classificationInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetClasificationsAsync(_classificationInput);
        }

        private List<TipoCambio> GetTiposCambios()
        {
            var result = new List<TipoCambio>{
                new TipoCambio { codigo = "Menor", nombreTipoCambio = "Menor" },
                new TipoCambio { codigo = "Mayor", nombreTipoCambio = "Mayor" }
            };
            return result;
        }

        private async Task<CrearDocumento> GetWFDocumentCreateAsync(string idPaso, string id)
        {
            _wfDocumentSelectInput.url_paramValue1 = idPaso;
            _wfDocumentSelectInput.url_paramValue2 = id;
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentSelectInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetDocumentCreateAsync(_wfDocumentSelectInput);
        }

        private async Task<string> WFDocumentManagerAsync(string strSerializeObject, bool IsSave = false)
        {   
            var wfDocumentManagerInput = _wfDocumentSaveApproveRejectInput; // -:> Solucionar el problema de doble copiado en memoria de los valores de wfDocumentManagerInput y al mismo tiempo se copian dichos valores en _wfDocumentSaveApproveRejectInput

            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                wfDocumentManagerInput.access_token = SessionState.Current.Session.GetString("access_token");

            wfDocumentManagerInput.url = _wfDocumentSaveApproveRejectInput.url + "?" + _wfDocumentSaveApproveRejectInput.url_paramKey1 + "=" + _wfDocumentSaveApproveRejectInput.url_paramValue1;

            if (IsSave)
                wfDocumentManagerInput.url = _wfDocumentCreateInput.url;
            
            return await Common.PostDocumentManagerAsync(wfDocumentManagerInput, strSerializeObject);
        }

        private async Task<string> WFDocumentCreateAsync(CrearDocumento crearDocumento)
        {   
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentCreateInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.PostDocumentCreateAsync(_wfDocumentCreateInput, crearDocumento);
        }

        private async Task<string> WFDocumentAnnexedAsync(string strSerializeObject)
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentAnnexedInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.PostDocumentAnnexedAsync(_wfDocumentAnnexedInput, strSerializeObject);
        }

        private async Task<string> WFDocumentSaveApproveRejectAsync(string strSerializeObject) 
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentSaveApproveRejectInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.PostDocumentSaveApproveRejectAsync(_wfDocumentSaveApproveRejectInput, strSerializeObject);
        }

        private async Task<string> WFDocumentUpdateAsync(string strSerializeObject)
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentSaveApproveRejectInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.PutDocumentUpdateAsync(_wfDocumentSaveApproveRejectInput, strSerializeObject);
        }

        private async Task<List<ListadoConsultaDocumentos>> ListDocsQueueAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentSelectListDocsInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetListDocsQueueAsync(_wfDocumentSelectListDocsInput);
        }

        private async Task<List<ReportMatrizRetenciones>> MatrizRetencionesAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentReportMatrizRetencionesInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetReportMatrizRetencionesAsync(_wfDocumentReportMatrizRetencionesInput);
        }

        private async Task<List<ReportListadoMaestroDocs>> ListadoMaestroDocsAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentReportListadoMaestroDocsInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetReportListMastDocsAsync(_wfDocumentReportListadoMaestroDocsInput);
        }

        private async Task<List<ReportListadoDocsObsoletos>> ListadoDocsObsoletosAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfDocumentReportListadoDocsObsoletosInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetReportListObsoletesDocsAsync(_wfDocumentReportListadoDocsObsoletosInput);
        }
    }

}