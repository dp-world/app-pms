﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using App_Pms.Web.DPWApiClient.Workflow.Manager;
using App_Pms.Web.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace App_Pms.Web.Controllers
{
    [Route("api/document")]
    public class DocumentRestController : Controller
    {
        private readonly string _strIP;
        private readonly string _urlReset;
        private readonly IPInput _ipInput;
        private readonly WFDocumentSearchDocsNameInput _wfDocumentSearchDocsNameInput;

        public DocumentRestController(
            IOptions<IPInput> ipInput, 
            IOptions<WFDocumentSearchDocsNameInput> wfDocumentSearchDocsNameInput
            )
        {
            _ipInput = ipInput.Value;
            _wfDocumentSearchDocsNameInput = wfDocumentSearchDocsNameInput.Value;
            _strIP = _ipInput.flag_url_dpw.Equals("0") ? _ipInput.url_dev : (_ipInput.flag_url_dpw.Equals("1") ? _ipInput.url_prd : string.Empty);
            _wfDocumentSearchDocsNameInput.url = _wfDocumentSearchDocsNameInput.url.Replace("{IP}", _strIP);
            _urlReset = _wfDocumentSearchDocsNameInput.url.Split("=")[0] + "=" + _wfDocumentSearchDocsNameInput.docnameKey;
            _urlReset+= "&" + (_wfDocumentSearchDocsNameInput.url.Split("=")[1]).Split("&")[1] + "=" + _wfDocumentSearchDocsNameInput.obsoleteKey;
            _wfDocumentSearchDocsNameInput.url = _urlReset;
        }

        //Local Test:=> https://localhost:44335/Document/api/document/searchdocname?term=a
        [Produces("application/json")]
        [HttpGet("searchdocname")]
        public async Task<IActionResult> SearchDocumentName()
        {
            try
            {   
                if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                    _wfDocumentSearchDocsNameInput.access_token = SessionState.Current.Session.GetString("access_token");

                string term = HttpContext.Request.Query["term"].ToString();
                _wfDocumentSearchDocsNameInput.url = _wfDocumentSearchDocsNameInput.url.Replace(_wfDocumentSearchDocsNameInput.docnameKey, term);
                _wfDocumentSearchDocsNameInput.url = _wfDocumentSearchDocsNameInput.url.Replace(_wfDocumentSearchDocsNameInput.obsoleteKey, "false");
                var names = await Common.GetListSearchDocsNamesAsync(_wfDocumentSearchDocsNameInput);
                return Ok(names);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}