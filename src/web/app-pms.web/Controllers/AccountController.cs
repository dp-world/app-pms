﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using App_Pms.Web.Models;
using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities;
using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Manager;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;

namespace App_Pms.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IPInput _ipInput;
        private readonly TokenInputJwt _tokenInputJwt;
        private readonly UserInfoInput _userInfoInput;

        public AccountController(IOptions<IPInput> ipInput, IOptions<TokenInputJwt> tokenInputJwt, IOptions<UserInfoInput> userInfoInput)
        {
            _ipInput = ipInput.Value;
            _tokenInputJwt = tokenInputJwt.Value;
            _userInfoInput = userInfoInput.Value;
            var _strIP = _ipInput.flag_url_dpw.Equals("0") ? _ipInput.url_dev : (_ipInput.flag_url_dpw.Equals("1") ? _ipInput.url_prd : string.Empty);
            _tokenInputJwt.url = _tokenInputJwt.url.Replace("{IP}", _strIP);
            _userInfoInput.url = _userInfoInput.url.Replace("{IP}", _strIP);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel _loginViewModel, string returnUrl)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = await BuildToken(_loginViewModel);
                    if (result.StatusCode.Equals("OK"))
                        if (!string.IsNullOrEmpty(result.access_token))
                        {
                            _loginViewModel.TokenOutputJwt = result;
                            var _userInfo = await GetUserInfo(_loginViewModel.TokenOutputJwt);
                            HttpContext.Session.SetString("access_token", _loginViewModel.TokenOutputJwt.access_token);
                            HttpContext.Session.SetString("user_name", _loginViewModel.User);
                            HttpContext.Session.SetString("full_name", _userInfo.NombreCompleto);
                            HttpContext.Session.SetString("department", _userInfo.Departamento);
                            HttpContext.Session.SetString("email", _userInfo.CorreoElectronico);

                            ViewData["UserName"] = _userInfo.NombreUsuario;
                            ViewData["FullName"] = _userInfo.NombreCompleto;

                            #region snippet1
                            var claims = new List<Claim>
                            {
                                new Claim(ClaimTypes.Name, _loginViewModel.User),
                                new Claim("FullName", _userInfo.NombreCompleto),
                                new Claim("Department", _userInfo.Departamento),
                                new Claim("Email", _userInfo.CorreoElectronico),
                                new Claim(ClaimTypes.Role, "Administrator"),
                            };

                            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                            var authProperties = new AuthenticationProperties
                            {
                                //AllowRefresh = <bool>,
                                // Refreshing the authentication session should be allowed.

                                //ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                                // The time at which the authentication ticket expires. A 
                                // value set here overrides the ExpireTimeSpan option of 
                                // CookieAuthenticationOptions set with AddCookie.

                                //IsPersistent = true,
                                // Whether the authentication session is persisted across 
                                // multiple requests. When used with cookies, controls
                                // whether the cookie's lifetime is absolute (matching the
                                // lifetime of the authentication ticket) or session-based.

                                //IssuedUtc = <DateTimeOffset>,
                                // The time at which the authentication ticket was issued.

                                //RedirectUri = <string>
                                // The full path or absolute URI to be used as an http 
                                // redirect response value.
                            };

                            await HttpContext.SignInAsync(
                                CookieAuthenticationDefaults.AuthenticationScheme,
                                new ClaimsPrincipal(claimsIdentity),
                                authProperties);
                            #endregion

                            if (!string.IsNullOrEmpty(returnUrl))
                                return Redirect(returnUrl);

                            return RedirectToAction("Index", "Home");
                        }

                    if (!result.ErrorMessage.Equals(string.Empty))
                        ModelState.AddModelError(string.Empty, "X( Error en el servidor, por favor comunicarse con el administrador del sistema :(");

                    if (result.error.Equals("unauthorized"))
                        ModelState.AddModelError(string.Empty, "Usuario y/o contraseña inválidos.");

                    if (result.error.Equals("server_error") || result.StatusCode.Equals("InternalServerError"))
                        ModelState.AddModelError(string.Empty, "Error Interno, favor informar al administrador del sistema.");
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "Error Interno, favor informar al administrador del sistema. [" + ex.Message + "]");
            }
            return View(_loginViewModel);
        }

        // GET: /Account/LogOff 
        [HttpGet]
        public async Task<IActionResult> LogOff()
        {
            DestroyToken();
            await LogOffAsync();
            return RedirectToAction("Login");
        }

        private void DestroyToken()
        {
            HttpContext.Session.SetString("access_token", string.Empty);
            HttpContext.Session.Clear();
            HttpContext.Session.Remove("access_token");
            HttpContext.Session=null;
        }

        public async Task<String> LogOffAsync()
        {
            await Task.Delay(5);

            // Clear the existing external cookie
            #region snippet2
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);
            #endregion

            return "LogOff";
        }

        private async Task<TokenOutputJwt> BuildToken(LoginViewModel loginViewModel) 
        {
            _tokenInputJwt.body_val2 = loginViewModel.User;
            _tokenInputJwt.body_val3 = loginViewModel.Password;
            _tokenInputJwt.body_val4 = loginViewModel.RememberMe;
            return await SignInJwt.PasswordSignInJwtAsync(_tokenInputJwt, loginViewModel.RememberMe, lockoutOnFailure: true);
        }

        private async Task<UserInfoJwt> GetUserInfo(TokenOutputJwt tokenOutputJwt) 
        {
            return await SignInJwt.GetUserInfoAsync(tokenOutputJwt, _userInfoInput);
        }
    }
}