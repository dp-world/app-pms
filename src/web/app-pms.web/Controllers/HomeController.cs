﻿using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using App_Pms.Web.Models;
using App_Pms.Web.Security;
using App_Pms.Web.DPWApiClient.Workflow.Entities;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using App_Pms.Web.DPWApiClient.Workflow.Manager;
using System;
using Microsoft.AspNetCore.Authorization;

namespace App_Pms.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        
        private readonly ILogger<HomeController> _logger;
        private readonly IPInput _ipInput;
        private readonly WFQueueInput _wfQueueInput;
        private readonly WFStatisticsInput _wfStatisticsInput;
        private readonly string _strIP;

        public HomeController(ILogger<HomeController> logger, IOptions<IPInput> ipInput, IOptions<WFQueueInput> wfQueueInput, IOptions<WFStatisticsInput> wfStatisticsInput)
        {   
            _logger = logger;
            _ipInput = ipInput.Value;
            _wfQueueInput = wfQueueInput.Value;
            _wfStatisticsInput = wfStatisticsInput.Value;
            
            _strIP = _ipInput.flag_url_dpw.Equals("0") ? _ipInput.url_dev : (_ipInput.flag_url_dpw.Equals("1") ? _ipInput.url_prd : string.Empty);

            _wfQueueInput.url = _wfQueueInput.url.Replace("{IP}", _strIP);
            _wfStatisticsInput.url = _wfStatisticsInput.url.Replace("{IP}", _strIP);
        }

        [SessionTimeout]
        public async Task<IActionResult> Index()
        {
            try
            {
                if ((!string.IsNullOrEmpty(SessionState.Current.Session.GetString("WFDocument"))) && (!SessionState.Current.Session.GetString("WFDocument").Equals("Error")))
                    ViewData["WFDocument"] = SessionState.Current.Session.GetString("WFDocument");

                HttpContext.Session.SetString("WFDocument", string.Empty);

                var statistics = await StatisticsBoardAsync(); var defaultValue = "TBD";
                if (!string.IsNullOrEmpty(statistics.result))
                    return RedirectToAction("Error", "Home", new { @statusCode = statistics.result.Split("|")[0], @message = statistics.result.Split("|")[1] });

                ViewData["CantidadCreacion"] = string.IsNullOrEmpty(statistics.CantidadCreacion) ? defaultValue : statistics.CantidadCreacion;
                ViewData["CantidadActualizacion"] = string.IsNullOrEmpty(statistics.CantidadActualizacion) ? defaultValue : statistics.CantidadActualizacion;
                ViewData["CantidadObsoletizacion"] = string.IsNullOrEmpty(statistics.CantidadObsoletizacion) ? defaultValue : statistics.CantidadObsoletizacion;
                ViewData["CantidadReactivacion"] = string.IsNullOrEmpty(statistics.CantidadReactivacion) ? defaultValue : statistics.CantidadReactivacion;
                ViewData["CantidadNomenclatura"] = string.IsNullOrEmpty(statistics.CantidadNomenclatura) ? defaultValue : statistics.CantidadNomenclatura;

                var _bandejaTrabajo = await WorkQueueAsync();

                if (_bandejaTrabajo == null)
                    return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = "Error obteniendo la cola de trabajo." });
                
                if (string.IsNullOrEmpty(_bandejaTrabajo.result))
                    return View(_bandejaTrabajo.ListadoBandejaTrabajo.ToList());

                return RedirectToAction("Error", "Home", new { @statusCode = _bandejaTrabajo.result.Split("|")[0], @message = _bandejaTrabajo.result.Split("|")[1] });
            }
            catch (Exception ex)
            {
                return RedirectToAction("Error", "Home", new { @statusCode = 500, @message = ex.Message });
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error(int? statusCode = null, string message = "")
        {
            ErrorViewModel error = null;
            if (statusCode.HasValue)
            {
                error = new ErrorViewModel
                {
                    RequestId = statusCode.ToString(), RequestMessage = message
                };
            }
            return View(error);
        }

        private async Task<BandejaTrabajo> WorkQueueAsync()
        {   
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfQueueInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetWorkQueueAsync(_wfQueueInput);
        }

        private async Task<TableroEstadisticas> StatisticsBoardAsync()
        {
            if (!string.IsNullOrEmpty(SessionState.Current.Session.GetString("access_token")))
                _wfStatisticsInput.access_token = SessionState.Current.Session.GetString("access_token");

            return await Common.GetStatisticsBoardAsync(_wfStatisticsInput);
        }
    }
} 