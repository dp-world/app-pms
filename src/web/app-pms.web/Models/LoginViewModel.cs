﻿using System.ComponentModel.DataAnnotations;
using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities;

namespace App_Pms.Web.Models
{
    public class LoginViewModel
    {
        [Required]        
        public string User { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Recordarme")]
        public bool RememberMe { get; set; }

        public TokenOutputJwt TokenOutputJwt { get; set; }
    }
}