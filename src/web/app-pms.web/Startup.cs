using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App_Pms.Web.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using App_Pms.Web.DPWApiClient.Security.Authentication.Jwt.Entities;
using App_Pms.Web.DPWApiClient.Workflow.Entities.Input;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace App_Pms.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer();

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(165);
            });

            #region snippet1
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            #endregion

            services.AddMvc();

            services.AddHttpContextAccessor();
            
            services.Configure<IPInput>(Configuration.GetSection("DPWApiClient:IP"));
            services.Configure<TokenInputJwt>(Configuration.GetSection("DPWApiClient:Authentication"));
            services.Configure<UserInfoInput>(Configuration.GetSection("DPWApiClient:UserInfo"));
            services.Configure<WFQueueInput>(Configuration.GetSection("DPWApiClient:WorkQueues"));
            // Listas Desplegables {
            services.Configure<DepartmentInput>(Configuration.GetSection("DPWApiClient:Departments"));
            services.Configure<DocumentaryTypologyInput>(Configuration.GetSection("DPWApiClient:DocumentaryTypologies"));
            services.Configure<CreationReasonInput>(Configuration.GetSection("DPWApiClient:CreationReasons"));
            services.Configure<RetentionTimeInput>(Configuration.GetSection("DPWApiClient:RetentionTimes"));
            services.Configure<StorageMediaInput>(Configuration.GetSection("DPWApiClient:StorageMedia"));
            services.Configure<ReviewFrequencyInput>(Configuration.GetSection("DPWApiClient:ReviewFrequency"));
            services.Configure<ClassificationInput>(Configuration.GetSection("DPWApiClient:Classifications"));
            // Listas Desplegables }
            services.Configure<WFDocumentCreateInput>(Configuration.GetSection("DPWApiClient:WFDocumentCreate"));
            services.Configure<WFDocumentSelectInput>(Configuration.GetSection("DPWApiClient:WFDocumentSelect"));
            services.Configure<WFDocumentSelectListDocsInput>(Configuration.GetSection("DPWApiClient:WFDocumentSelectListDocs"));
            services.Configure<WFDocumentSaveApproveRejectInput>(Configuration.GetSection("DPWApiClient:WFDocumentSaveApproveReject"));
            services.Configure<WFDocumentDownloadInput>(Configuration.GetSection("DPWApiClient:WFDocumentDownload"));
            services.Configure<WFDocumentTemplatesInput>(Configuration.GetSection("DPWApiClient:WFDocumentTemplates"));
            services.Configure<WFDocumentAnnexedInput>(Configuration.GetSection("DPWApiClient:WFDocumentAnnexed"));
            services.Configure<WFDocumentSearchDocsNameInput>(Configuration.GetSection("DPWApiClient:WFDocumentSearchDocsName"));
            services.Configure<WFDocumentReportMatrizRetencionesInput>(Configuration.GetSection("DPWApiClient:WFDocumentReportMatrizRetenciones"));
            services.Configure<WFDocumentReportListadoMaestroDocsInput>(Configuration.GetSection("DPWApiClient:WFDocumentReportListadoMaestroDocs"));
            services.Configure<WFDocumentReportListadoDocsObsoletosInput>(Configuration.GetSection("DPWApiClient:WFDocumentReportListadoDocsObsoletos"));
            services.Configure<WFStatisticsInput>(Configuration.GetSection("DPWApiClient:WFStatistics"));
            services.Configure<LastStepInput>(Configuration.GetSection("DPWApiClient:LastStep"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseStatusCodePagesWithReExecute("/Home/Error", "?statusCode={0}");
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();

            SessionState.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}