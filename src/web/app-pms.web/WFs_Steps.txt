﻿Creación
#1 - 000 - Solicitante
#2 - 203 - Revisión (área de procesos)
#3 - 204 - Aprobación (Gerente de área)
#4 - 205 - Aprobacion (Director de área)
#5 - 401 - Aprobación (Gerente/Supervisor de procesos)
?? - 402 ???
#6 - 403 - Revisión Final (área de procesos)

Actualización
#1 - 000 - Solicitante
#2 - 602 - Envía / Habilita documento solicitado (área de procesos)
#3 - 603 - Realizar cambios a documento (Productor)
#4 - 604 - Verificar documento y validar la información contenida (área de procesos)
#5 - 605 - Verificar documento y validar la información contenida (Gerente de área)
#6 - 606 - Verificar documento y validar la información contenida (Director de área)
#7 - 607 - Verificar documento y validar la información (Gerente de procesos)
#8 - 608 - Ajustes finales de documento y aprobación (área de procesos)

Obsoletización
#1 - 000 - Solicitante
#2 - 802 - Verificar documento (área de procesos)
#3 - 803 - Verificar documento (Gerente de área)
#4 - 804 - Rechazar Obsoletización (área de procesos)
#5 - 805 - Verificar documento (Director de área)
#6 - 806 - Verificar documento (Gerente/Supervisor de procesos)
#7 - 807 - Confirmar obsoletización (área de procesos)

Reactivación
#1 - 000 - Solicitante
#2 - 1004 - [Revisión (área de procesos)]
#3 - 1008 - [Aprobación (Gerente de área)]
#4 - 1009 - [Aprobación (Director de área)]
#5 - 1010 - [Aprobación (Gerente de procesos)]
#6 - 1011 - [Confirmar reactivación (área de procesos)]

Nomenclatura
#1 000  - Solicitante
#2 1202 - Revisión (área de procesos)
#3 1601 - Realizar cambios a documento (Solicitante)								=> Debe poder descargar el documento habilitado por el area de procesos (paso 1202) y Debe poder cargar/actualizar el documento 
#4 1602 - Verificar documento y validar la información contenida (área de procesos) => Debe poder descargar el documento cargado por el solicitante (paso 1601)
#5 1401 - Aprobación (Gerente de área)												=> Debe poder descargar el documento para aprobar y/o rechazar
#6 1402 - Aprobación (Director de área)												=> Debe poder descargar el documento para aprobar y/o rechazar
#7 1603 - Verificar documento (Gerente/Supervisor de procesos)						=> Debe poder descargar el documento para aprobar y/o rechazar
#8 1403 - Revisión Final (área de procesos)											=> Debe poder descargar el documento para finalizar la solicitud

weblogic
WCCDpW0rldPro$2018

Sarias
DEBUGDEBUG